<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        #$this->middleware('auth');
    }

    public function index()
    {
		$data['banner'] 		= DB::select('select * from `imagetable` where status = 2');
		$data['gallery'] 		= DB::select('select * from `imagetable` where status = 3');
		$data['testimonials'] 	= DB::select('select * from `imagetable` where status = 4');
		$data['doctors'] 		= DB::select('select * from `imagetable` where status = 5');
		
		$data['services']		= DB::select('select * from `imagetable` where id = 13');
		$data['departments']	= DB::select('select * from `imagetable` where status = 6');
		$data['ourclinic']		= DB::select('select * from `imagetable` where status = 7');
		$data['offer']			= DB::select('select * from `imagetable` where status = 8');
		
		$data['homepagecms']	= DB::select('select * from `imagetable` where id = 48');
		$data['homepagecms01']	= DB::select('select * from `imagetable` where id = 120');
		
		$data['pagename']	= 'homepage';
        return view('home',$data);
    }

    public function drzena()
    {
		$data['banner']	= DB::select('select * from `imagetable` where id = 4');
		$data['services']		= DB::select('select * from `imagetable` where id = 13');
		$data['cms']	= DB::select('select * from `imagetable` where id = 49');
		$data['pagename']	= 'drzena';
        return view('drzena',$data);
    }

	
	
	public function about()
	{
		$data['banner']	= DB::select('select * from `imagetable` where id = 4');
		$data['doctors'] 		= DB::select('select * from `imagetable` where status = 5');
		$data['testimonials'] 	= DB::select('select * from `imagetable` where status = 4');
		$data['partner']		= DB::select('select * from `imagetable` where status = 9');
		$data['cms']	= DB::select('select * from `imagetable` where id = 50');
		$data['pagename']		= 'aboutpage';
		//echo '<pre>';print_r($data);exit;
		return view('about',$data);
	}
	
	public function services()
	{
		$data['banner']		= DB::select('select * from `imagetable` where id = 4');
		$data['service01']	= DB::select('select * from `imagetable` where status = 15');
		$data['otherservice01']	= DB::select('select * from `imagetable` where status = 16');
		$data['pagename']	= 'sericespage';
		/*
		if(!empty($_REQUEST['xyz'])){
			return view('testpage',$data);
		}else{}*/
			return view('services',$data);
		
		
	}
	
	public function gallery()
	{
		$data['banner']	= DB::select('select * from `imagetable` where id = 5');
		$data['gallery']	= DB::select('select * from `imagetable` where status = 15');
		$data['pagename']	= 'gallerypage';
		return view('gallery',$data);
	}
	
	public function contact()
	{
		$data['banner']	= DB::select('select * from `imagetable` where id = 4');
		$data['pagename']	= 'contactpage';
		//print_r($data);
		return view('contact',$data);
	}
	
	public function bookappoinment(Request $req){
		$patient_name		= $req->patient_name;
		$patient_contact	= $req->patient_contact;
		$patient_date		= $req->patient_date;
		$patient_time		= $req->patient_time;
		$patient_message	= $req->patient_message;
		
		$sql = "select * from  appointment where sel_dtd = '".$patient_date."' and sel_time = '".$patient_time."'";
		$res = DB::select($sql);
		$cnt = 0;
		foreach($res as $val){
			$cnt++;
		}
		$msg = base64_encode(date('m/d/Y',strtotime($patient_date)).': '.$patient_time.':00 is not booked by another patient ..please select another time');
		if($cnt == 0){
			$sql = "INSERT INTO appointment(fname,contact,sel_dtd,sel_time,msg,status) VALUES('".$patient_name."','".$patient_contact."','".$patient_date."','".$patient_time."','".$patient_message."',1)";
			DB::insert($sql);
			$msg = base64_encode('Appointment successfully scheduled for '.date('m-d-Y',strtotime($patient_date)).': '.$patient_time.':00.');
		}
		/**/
		header("Location:http://doctor.upticks.io/?xyz=1&msg=".$msg);
		exit;
	}
	
}
