<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="assets/images//favicon.ico" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Zena Dental PLLC</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/newapp.css') }}" rel="stylesheet">


	<link rel="stylesheet" type="text/css" href="assets/revolution/fonts/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="assets/revolution/fonts/font-awesome/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Zena Dental PLLC
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <?php /*<li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>*/?>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
									<li><a href="/admin/appointment">Appointment Banner Management</a></li>									
									<li><a href="/admin/?page=banner">Banner Management</a></li>
									<li><a href="/admin/?page=cms">Content Management System</a></li>
									<li><a href="/admin/?page=testimonials">Testimonials Management System</a></li>
									<li><a href="/admin/?page=service">Service Management System</a></li>
									<li><a href="/admin/?page=gallery">Home Page Gallery Management</a></li>
									<li><a href="/admin/?page=others" style="display:none;">Other Pages</a></li>
									<li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
<div class="modal fade" id="addedit" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<?php /*{{ Form::open(array('url' => 'admin/updatedata')) }}
			{{ Form::close() }}	*/ ?>
			<form action="/admin/updatedata" method="post" id="frm"  enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="favoritesModalLabel" style="color:#6c27ab;">Update Data...</h4>
				</div>				
				<div class="modal-body">
					<div style="display:none;">
					<input type="text" name="id" id="id_data" value="0" style="width:80%; color:#000; display:none;" />
					<input type="text" name="type" id="type_data" value="0" style="width:80%; color:#000; display:none;" />
					<textarea class="form-control" id="content02" name="content02" style="display:none;"></textarea>
					</div>
					<input type="file" name="imgname" id="imgname"  />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> &nbsp; &nbsp;
					<span class="pull-right">
						<button type="submit" class="btn btn-primary">
							Update
						</button>
					</span>
				</div>
			</form>
		</div>
	</div>
</div> 
    <script src="{{ asset('js/app.js') }}"></script>
	<!-- Scripts --> 
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
	<script>
		CKEDITOR.replace( 'content02' );
	</script>
	<script>
		function addgalleryimage(){
			$('#imgname').show();
		}
		function testjs(){alert(99);}
		function editbanner(x,y){
			//$('#type_data').val(0);
			//$('#id_data').val(x);
			$('#imgname').show();
			//CKEDITOR.instances['content02'].setData(y);
		}
		function editcontent(x,y,type){
			$('#id_data').val(x); 
			$('#type_data').val(type);
			$('#imgname').hide();
			CKEDITOR.instances['content02'].setData(y);
		}
	</script>
	<script src="{{ asset('adminjs.js') }}"></script>
	
</body>
</html>
