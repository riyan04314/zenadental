@extends('layouts.template')

@section('content')



  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
		

			<!-- Page Banner -->
			<div class="page-banner container-fluid no-left-padding no-right-padding" style="background-image:url('/assets/uploadedimages/g11.jpg')">>
				<!-- Container -->
				<div class="container">
					<div class="page-banner-content">
						<h3> </h3>
					</div>
					<div class="banner-content">
						<ol class="breadcrumb">
							<li><a href="index.html">Home</a></li>
							<li class="active">services</li>
						</ol>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Banner -->

			<div class="offer-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3 style="color:#000;">Service Offerings </h3>
					</div><!-- Section Header /- -->
					<div class="row">
					
 
<div id="accordion">
  
<?php
	$arr = array('EXTRACTION','CHILDREN\'S DENTISTRY','IMPLANT DENTISTRY','PERIODONTICS','PREVENTING DENTISTRY','RESTORATIVE DENTISTRY','ROOT CANAL TREATMENT','TECHNOLOGY','TMJ THERAPY','COSMETICS DENTISTRY','ORTHODONTICS ( BRACES )');
	foreach($arr as $key=>$val){
?> 
  <h3><?php echo $val;?></h3>
  <div>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  </div>
<?php } ?>  
</div>
 
					</div>
				</div>
			</div>
 
			
			
			<!-- Offer Section -->
			<div class="offer-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3 style="color:#000;">Service Offerings </h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<?php
						
						?>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">General Dentistry </h5>
								<p style="text-align:initial;">We offer a wide range of general dental treatments, which include:</p>
								<p style="text-align:initial;">Dental Cleaning and Tooth Scaling</p>
								<p style="text-align:initial;">Tooth Restorations - Amalgam Fillings (silver) or Composite Fillings (white)</p>
								<p style="text-align:initial;">Permanent Crowns/Bridges, Dental Implants, Porcelain Veneers</p>
								<p style="text-align:initial;">Root Canal</p>
								<p style="text-align:initial;">Surgical Extractions</p>
								<p style="text-align:initial;">Digital Radiography</p>
								<p style="text-align:initial;">Dental Implants</p>
								<p style="text-align:initial;">Full/ Partial Dentures (Flexible or metal)</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">Cosmetic Dentistry</h5>
								<p style="text-align:initial;">Cosmetic Dentistry is the process of improving the aesthetic quality of a person’s smile.</p>
								<p style="text-align:initial;">A purely cosmetic procedure is one that is performed without a medical reason, solely to improve a patient’s appearance. Through cosmetic dentistry, we are able to redesign the functional aspects of the teeth so that they have a unified, uniform, natural look. We achieve this by using special non-metal materials, such as porcelain or composites, which allow        for a more natural looking smile.</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="offer-box" style="initial">
								<h5 style="center">Orthodontics DENTISTRY</h5>
<p style="text-align:initial;">The best and most conservative way to improve your smile and the health of your teeth is through orthodontic treatment. With either conventional orthodontics (clear or metal braces) or Invisalign treatment (clear removable trays), Zena dental will create for you a beautiful and natural smile. We specialize in orthodontics for patients of all ages.</p>
<p style="text-align:initial;">Dental Smile Makeovers</p>
<p style="text-align:initial;">Clear and Metal Braces</p>
<p style="text-align:initial;">Invisible Braces and Invisalign</p>
<p style="text-align:initial;">Invisalign Teen Braces</p>
<p style="text-align:initial;">Arrowlign / Fix Six</p>
							</div>
						</div>
					</div>
				</div><!-- Container -->
			</div><!-- Offer Section /-  -->
			
<style>
.department-section .department-img-block span{
	font-size:14px;
}
.department-section .department-box{
	width:20%;
}
</style>	
			
		@endsection