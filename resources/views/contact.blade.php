@extends('layouts.template')
@section('content')
	<!-- Page Banner -->
	<div class="page-banner container-fluid no-left-padding no-right-padding" style="background-image:url('/assets/uploadedimages/g11.jpg')">
		<!-- Container -->
		<div class="container">
			<div class="page-banner-content">
				<h3>&nbsp;</h3>
			</div>
			<div class="banner-content">
				<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li class="active">Contact</li>
				</ol>
			</div>
		</div>
	</div>

	
	<div class="contact-us container-fluid no-left-padding no-right-padding">
		<!-- Container -->
		<div class="container">
			<div class="contact-header">
				<h5>Get in touch with us</h5>
			</div>
			<div class="contact-form">
				<h5>Send us a message</h5>
				<form class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<input type="text" class="form-control" placeholder="Your Name" name="contact-name" id="input_name" required="" />
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<input type="text" class="form-control" placeholder="Your Phone" name="contact-phone" id="input_phone" required="" />
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<textarea class="form-control" placeholder="Message" rows="6" name="textarea-message" id="textarea_message"></textarea>
					</div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12">
						<button title="Submit" type="submit" id="btn_submit" name="post">Send</button>
					</div>
					<div id="alert-msg" class="alert-msg"></div>
				</form>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="contact-call-box">
						<p><i class="fa fa-phone" title="Phone"></i><a href="tel:4692720003" title="+(469) 272-0003">+(469) 272-0003</a></p>
					</div>
					<div class="contact-call-box">
						<p><i class="fa fa-fax" title="Fax"></i><a href="tel:4692720003" title="+(469) 272-0003">+(469)-272-0004</a></p>
					</div>
					<div class="contact-call-box">
						<p>
							<i class="fa fa-envelope" title="Email"></i>
							<a href="mailto:zenadental@hotmail.com" style="cursor:pointer;" title="zenadental@hotmail.com">
								zenadental@hotmail.com
							</a>
						</p>
					</div>
					<div class="contact-call-box">
						<p>
							<i class="fa fa-map-marker" title="Address"></i>
							445 E.FM 1382, Suite #6,<br>
							<span style="margin-left:65px;">
								Cedar Hill, TX 75104
							</span>
						</p>
					</div>
					<div class="contact-call-box">
						<p><i class="fa fa-heart"></i></p>
						<ul>
							<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>
							<li><a href="#" title="Vimeo"><i class="fa fa-vimeo"></i></a></li>
							<li><a href="#" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3361.5449141159497!2d-96.93812778482267!3d32.591658781028364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e8d9376058d1d%3A0xce0f7030d014004!2s445+FM1382+%236%2C+Cedar+Hill%2C+TX+75104%2C+USA!5e0!3m2!1sen!2sin!4v1562171221600!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
						<?php /*
						<div class="map-canvas" id="map-canvas-contact" data-lat="32.5916633" data-lng="-96.9381278" data-string="445 E.FM 1382, Suite #6, Cedar Hill, TX 75104" data-zoom="10"></div>
						*/ ?>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection			