@extends('layouts.template')

@section('content')



  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
		

			<!-- Page Banner -->
			<div class="page-banner container-fluid no-left-padding no-right-padding" style="background-image:url('/assets/uploadedimages/g11.jpg')">>
				<!-- Container -->
				<div class="container">
					<div class="page-banner-content">
						<h3> </h3>
					</div>
					<div class="banner-content">
						<ol class="breadcrumb">
							<li><a href="index.html">Home</a></li>
							<li class="active">services</li>
						</ol>
					</div>
				</div><!-- Container /- -->
			</div><!-- Page Banner -->

			<div class="offer-section container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3 style="color:#000;">Service Offerings </h3>
					</div><!-- Section Header /- -->
					<div class="row">
						<div id="accordion">
							<?php
								$arr = array('EXTRACTION','CHILDREN\'S DENTISTRY','IMPLANT DENTISTRY','PERIODONTICS','PREVENTING DENTISTRY','RESTORATIVE DENTISTRY','ROOT CANAL TREATMENT','TECHNOLOGY','TMJ THERAPY','COSMETICS DENTISTRY','ORTHODONTICS ( BRACES )');
								foreach($service01 as $key=>$val){
//print_r($val);exit;
							?> 
							  <h3><?php echo $val->content01;?></h3>
							  <div>
								<p><?php echo $val->content02;?></p>
							  </div>
							<?php } ?>  
						</div> 
					</div>
				</div>
			</div>
 
			
			<?php /*
			<!-- Offer Section -->
			<div class="offer-section container-fluid no-left-padding no-right-padding" style="padding-top:5px;">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="row">
						<?php
							foreach($otherservice01 as $val){
						?>
								<div class="col-md-4 col-sm-6 col-xs-6">
									<div class="offer-box" style="initial">
										
										<h5 style="center"><?php echo $val->content01;?></h5>
										<?php echo $val->content02;?>
									</div>
								</div>
						<?php
							}
						?>
					</div>
				</div><!-- Container -->
			</div><!-- Offer Section /-  -->
			
			 */?>
<style>
.department-section .department-img-block span{
	font-size:14px;
}
.department-section .department-box{
	width:20%;
}

.ui-draggable, .ui-droppable {
	background-position: top;
}
.ui-accordion .ui-accordion-header{
	padding:.5em .5em .5em 2.7em;
}
</style>	
			
		@endsection