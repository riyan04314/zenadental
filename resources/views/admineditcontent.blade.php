@extends('layouts.app')
@section('content')
<div class="page-banner container-fluid no-left-padding no-right-padding">
	<!-- Container -->
	<div class="container">
		<?php if(empty($_REQUEST['page'])){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li class="co-md-4"><a href="/admin/appointment">Appointment Banner Management</a></li>									
				<li class="co-md-4"><a href="/admin/?page=banner">Banner Management</a></li>
				<li class="co-md-4"><a href="/admin/?page=cms">Content Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=testimonials">Testimonials Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=service">Service Management System</a></li>
				<li class="co-md-4"><a href="/admin/?page=gallery">Home Page Gallery Management</a></li>			
			</ol>
		</div>
		<?php } ?>
		
		
		<div class="banner-content">
			<ol class="breadcrumb">
				<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'banner'){?>
					<li style="color: #000; font-size: large;">Banner Management System</li>
					<li style="float:right;"><a href="#" style="text-align:right" data-toggle="modal" data-target="#addedit" onclick="editbanner(0,'')" >Add More Banner</a></li>
				<?php } ?>
				<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'cms'){?>
					<li style="color: #000; font-size: large;">Content Management System</li>
				<?php } ?>
		
				<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'testimonials'){?>
				<li style="color: #000; font-size: large;">Testimonials Section For Home Pages</li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Testimonials</a></li>	
				<?php } ?>	
				<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'service'){?>
						<li style="color: #000; font-size: large;">Service Offerings Management System</li>
						<li style="float:right;"><a href="#" style="text-align:right" data-toggle="modal" data-target="#addedit" onclick="editbanner(0,'')" >Add More Service</a></li>
				<?php } ?>				
			</ol>
		</div>
		
		<div class="banner-content">
			<form action="/admin/updatedata" method="post" id="frm"  enctype="multipart/form-data">
			{{ csrf_field() }}
			<table width="100%" style="border:0px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<td>Content</td>
					</tr>
					<tr>
						<td style="padding:2% 0;">
							<input type="text" name="id" id="id_data" value="<?php echo $contentid;?>" style="width:80%; color:#000; display:none;" />
							<input type="text" name="type" id="type" value="<?php echo $type;?>" style="width:80%; color:#000; display:none;" />
							<input type="text" name="content01" id="content01" value="<?php echo $content01;?>" style="width:80%; color:#000; padding:1%; display:none1;" />							
						</td>
					</tr>
					<tr>
						<td>
							<textarea class="form-control" id="content02" name="content02"><?php echo $content02;?></textarea>
						</td>
					</tr>
					<tr>
						<td style="padding:4%;">
							<input type="file" name="imgname" id="imgname" style="padding:2%;"  />
							<?php if(!empty($imgname)){?>
							<img src="<?php echo '/assets/uploadedimages/'.$imgname;?>" width="50%"/>
							<?php }?>
						</td>
					</tr>
					<tr>
						<td>
							<?php //print_r($content[0]);?>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> &nbsp; &nbsp;
							<span class="pull-right1">
								<input type="submit" class="btn btn-primary" value="Update" >
							</span>
						</td>
					</tr>
					</tr>
				</thead>
			</table>
			</form> 
		</div>
		<br><br>
	
		
		

		

		

		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'gallery'){?>
		
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Gallery Image Management</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Gallery Image</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($gallery as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="120" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						} 
					?>
				</tbody>
			</table>
		</div>
		
		<br><br>
		<?php } ?>
		
		<?php if(!empty($_REQUEST['page']) && $_REQUEST['page'] == 'others'){?>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Doctors Section For Home Pages</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Doctors</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($doctors as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="120" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		

		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Departments Section For Home Pages</a></li>
				<li style="float:right;"><a href="#" style="text-align:right" >Add More Departments</a></li>			
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($departments as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="80" style="margin:10px; border:1px solid #CCC; background-color:#FFF;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		


		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Clinic Section For Home Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($clinic as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Offer Section For Home Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($offer as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5>'.$val->content01.'</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		

		
		<br><br>
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Banner Section For Other Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px;">Content</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($pagesbanner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="100" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;"><h5>'.$val->image_content.'<h5></td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		
		<?php } ?>
		<br><br>
		
		
		<?php /*
		<div class="banner-content">
			<ol class="breadcrumb">
				<li><a href="#">Partner Section For About Us Pages</a></li>
			</ol>
		</div>
		<div class="banner-content">
			<table width="100%" style="border:1px solid #CCC; collapse:collapse;" border="1">
				<thead>
					<tr>
						<th style="padding:5px; width:40px;">SL.</th>
						<th style="padding:5px; width:300px;">Image</th>
						<th style="padding:5px; width:100px;">Action</th>
						<th style="padding:5px; width:100px;">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 0;
						foreach($partner as $val){
							$i++;
							echo '<tr>
									<td style="padding:5px;">'.$i.'</td>
									<td style="padding:5px;">
										<img src="/assets/uploadedimages/'.$val->imgname.'"  height="100" style="margin:10px; border:1px solid #CCC;" />
									</td>
									<td style="padding:5px;">
										<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#favoritesModal">Edit</button>
									</td>
									<td style="padding:5px;">Active</td>
								</tr>';	
						}
					?>
				</tbody>
			</table>
		</div>
		<br><br>
		
		*/?>
		
	</div>
</div>


@endsection